# ws vpc_endpoints /locals.tf
#test

locals {
  vpc_endpoints = {
    s3 = {
      vpc_id         = data.aws_vpc.vpc.id
      service_name   = "com.amazonaws.${local.region}.s3"
      route_table_id = data.aws_route_table.default_rt.id
    }
  }
}

locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
  }
}

locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
  }
}

locals {
  vpc_interface_endpoints = {
    ecr = {
      vpc_id             = data.aws_vpc.vpc.id
      subnet_ids         = local.subnets.private
      service_name       = "com.amazonaws.${local.region}.ecr.dkr"
      security_group_ids = module.security_groups.security_groups["vpc_interface_endpoints"].*.id
    }
    ecr_api = {
      vpc_id             = data.aws_vpc.vpc.id
      subnet_ids         = local.subnets.private
      service_name       = "com.amazonaws.${local.region}.ecr.api"
      security_group_ids = module.security_groups.security_groups["vpc_interface_endpoints"].*.id
    }
    cloudwatch = {
      vpc_id             = data.aws_vpc.vpc.id
      subnet_ids         = local.subnets.private
      service_name       = "com.amazonaws.${local.region}.logs"
      security_group_ids = module.security_groups.security_groups["vpc_interface_endpoints"].*.id
    }
    secretsmanager = {
      vpc_id             = data.aws_vpc.vpc.id
      subnet_ids         = local.subnets.private
      service_name       = "com.amazonaws.${local.region}.secretsmanager"
      security_group_ids = module.security_groups.security_groups["vpc_interface_endpoints"].*.id
    }
    rds = {
      vpc_id             = data.aws_vpc.vpc.id
      subnet_ids         = local.subnets.private
      service_name       = "com.amazonaws.${local.region}.rds"
      security_group_ids = module.security_groups.security_groups["vpc_interface_endpoints"].*.id
    }
  }
}

locals {
  security_groups = {
    vpc_interface_endpoints = {
      name        = "vpc_interface_endpoints_sg"
      vpc_id      = data.aws_vpc.vpc.id
      description = "Security group for private access to vpc endpoints"
      ingress_cidr = {
        http = {
          # from        = 443
          # to          = 443
          # protocol    = "tcp"
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = local.cidr_blocks.private
        }
      }
      ingress_sg = {}
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}


locals {
  region = data.aws_region.current.name
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}
