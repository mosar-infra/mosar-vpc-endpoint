# ws vpc endpoints datasources
# test

data "aws_vpc" "vpc" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_region" "current" {}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}
data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_route_table" "default_rt" {
  tags = {
    Name         = "mosar_default_private_route_table"
    Environment  = var.environment
  }
}
