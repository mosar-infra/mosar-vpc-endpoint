# ws vpc endpoint output
# test

output "vpc_endpoint" {
  value = module.vpc-endpoint.vpc_endpoint
}

output "vpc_interface_endpoint" {
  value = module.vpc-endpoint.vpc_interface_endpoint
}
output "security_groups" {
  value = module.security_groups
}
