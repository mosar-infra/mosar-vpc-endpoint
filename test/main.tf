# ws vpc endpoint /main.tf
# test

module "vpc-endpoint" {
  source                  = "git::https://gitlab.com/mosar-infra/tf-module-vpc-endpoint.git?ref=tags/v1.0.2"
  vpc_endpoints           = local.vpc_endpoints
  vpc_interface_endpoints = local.vpc_interface_endpoints
  environment             = var.environment
  managed_by              = var.managed_by
}

module "security_groups" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment     = var.environment
  managed_by      = var.managed_by
  security_groups = local.security_groups
}
